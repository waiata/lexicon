//
//  DocWindow.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class DocWindow: NSWindowController {
    
    @IBAction func showSpeak(_ sender: Any) {
        tabView?.selectTabViewItem(at: 0)
    }
    
    @IBAction func showCheck(_ sender: Any) {
        tabView?.selectTabViewItem(at: 1)
    }
    
    @IBAction func showTable(_ sender: Any) {
        tabView?.selectTabViewItem(at: 2)
    }
    
    @IBAction func showWrite(_ sender: Any) {
        tabView?.selectTabViewItem(at: 3)
    }

    var tabView: NSTabView? {
        return (contentViewController as? NSTabViewController)?.tabView
    }
}
