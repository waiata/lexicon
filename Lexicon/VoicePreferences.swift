//
//  VoicePreferences.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class VoicePreferences: NSViewController {

    @IBOutlet weak var voicePopup: NSPopUpButton!
    
    @IBAction func changeVoice(_ sender: NSPopUpButton) {
        guard let item = sender.selectedItem else { return }
        Default.voiceID = item.identifier?.rawValue
        Default.voiceName = item.title
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateVoices()
    }
    
    
    func populateVoices() {
        voicePopup.removeAllItems()
        for voice in NSSpeechSynthesizer.availableVoices {
            let atts = NSSpeechSynthesizer.attributes(forVoice: voice)
            let item = NSMenuItem()
            let name = atts[NSSpeechSynthesizer.VoiceAttributeKey.name] as! String
            let lang = atts[NSSpeechSynthesizer.VoiceAttributeKey.localeIdentifier] as! String
            let language = NSLocale.current.localizedString(forIdentifier: lang) ?? ""
            item.title = "\(name) \(language)"
            item.identifier = NSUserInterfaceItemIdentifier(voice.rawValue)
            voicePopup.menu?.addItem(item)
        }
        voicePopup.selectItem(withTitle: Default.voiceName ?? "")
    }
    
    struct Default {
        static let defaults = UserDefaults.standard
        
        static var voiceID: String? {
            get {
                return defaults.string(forKey: "VoiceID") ?? ""
            }
            set {
                UserDefaults.standard.set(newValue, forKey: "VoiceID")
            }
        }
        static var voiceName: String? {
            get {
                return defaults.string(forKey: "VoiceName") ?? ""
            }
            set {
                UserDefaults.standard.set(newValue, forKey: "VoiceName")
            }
        }
    }
}
