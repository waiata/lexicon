//
//  TableView.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class TableView: DocView, NSTableViewDelegate, NSTableViewDataSource {

    
    @IBOutlet weak var tableView: NSTableView!
    
    override func viewDidAppear() {
        super.viewDidAppear()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let word = words[row]
        let view = cell(for: word)
        return view
    }
    
    func cell(for word: Word) -> NSTableCellView? {
        let cell = tableView.makeView(withIdentifier: Table.wordID, owner: self) as? NSTableCellView
        cell?.textField?.stringValue = word.title
        return cell
    }
    
    struct Table {
        static let main = NSStoryboard(name: "Main", bundle: nil)
        
        static let wordID = NSUserInterfaceItemIdentifier("Word Cell")
        
    }
}
