//
//  DocView.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class DocView: NSViewController {
    
    var doc: Document? {
        return view.window?.windowController?.document as? Document
    }
    
    var words: [Word] {
        return doc?.words ?? []
    }
    
    var count: Int {
        return doc?.words.count ?? 0
    }
    
    var index: Int = 0 {
        didSet {
            if index < 0 { index = 0 }
            index = min(index, count)
            render()
        }
    }
    
    var canGoBack: Bool {
        return index > 1
    }
    
    var canGoNext: Bool {
        return index < count
    }
    
    var current: Word? {
        guard words.indices.contains(index - 1) else { return nil }
        return words[index - 1]
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        build()
    }
    
    func build() {
        
    }
    
    func render() {
        
    }
    
}
