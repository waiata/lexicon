//
//  Document.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class Document: NSDocument {
    
    var words = [Word]()
    
    var qwerty: String {
        return words.map{ $0.title }.joined(separator: "\r")
    }
    
    override class var autosavesInPlace: Bool {
        return true
    }
    
    override func makeWindowControllers() {
        self.addWindowController(UI.docWindow())
    }
    
    //MARK: File
    
    override func data(ofType typeName: String) throws -> Data {
        if let data = qwerty.data(using: .utf8) {
            return data
        } else {
            throw DocumentError.stringEncoding
        }
        
    }
    
    override func read(from data: Data, ofType typeName: String) throws {
        if let string = String(data: data, encoding: .utf8) {
            read(from: string)
        } else {
            throw DocumentError.stringDecoding
        }
    }
    
    /// read string and add each line as a Word
    func read(from string: String) {
        words = []
        for line in string.components(separatedBy: .newlines) {
            let word = Word(title: line)
            words.append(word)
        }
    }
    
    struct UI {
        static let main = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        
        static let docScene = NSStoryboard.SceneIdentifier("Document Window Controller")
        
        static func docWindow() -> NSWindowController {
            return main.instantiateController(withIdentifier: docScene) as! NSWindowController
        }
    }
    
    enum DocumentError: Error {
        case stringEncoding
        case stringDecoding
    }
    
    
}

