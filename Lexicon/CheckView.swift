//
//  CheckView.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class CheckView: DocView {
    
    @IBOutlet weak var label: NSTextField!
    @IBOutlet weak var display: NSTextField!
    
    @IBOutlet weak var backButton: NSButton!
    @IBOutlet weak var nextButton: NSButton!
    
    @IBAction func playPrevious(_ sender: Any) {
        index -= 1
        render()
        current?.speak()
    }
    @IBAction func playNext(_ sender: Any) {
        index += 1
        render()
        current?.speak()
    }
    
    @IBAction func restart(_ sender: Any) {
        build()
    }
    
    override func build() {
        index = 0
        render()
    }
    
    override func render() {
        label.stringValue = "\(index)/\(count)"
        display.stringValue = current?.title ?? ""
        backButton.isEnabled = canGoBack
        nextButton.isEnabled = canGoNext
    }
}
