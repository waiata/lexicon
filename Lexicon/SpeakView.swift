//
//  SpeakView.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class SpeakView: DocView {
    
    @IBOutlet weak var label: NSTextField!
    
    @IBOutlet weak var backButton: NSButton!
    @IBOutlet weak var nextButton: NSButton!
    
    @IBAction func playCurrent(_ sender: Any) {
        current?.speak()
    }
    
    @IBAction func playNext(_ sender: Any) {
        index += 1
        current?.speak()
    }
    
    @IBAction func playPrevious(_ sender: Any) {
        index -= 1
        current?.speak()
    }
    
    @IBAction func restart(_ sender: Any) {
        build()
    }
    
    var remaining = [Word]()
    
    override func build() {
        index = 0
        render()
    }
    
    override func render() {
        label.stringValue = "\(index)/\(count)"
        backButton.isEnabled = canGoBack
        nextButton.isEnabled = canGoNext
    }
}

