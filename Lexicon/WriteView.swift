//
//  WriteView.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class WriteView: DocView {

    @IBOutlet var textView: NSTextView!
    
    @IBAction func change(_ sender: Any) {
        guard let doc = self.doc else { return }
        doc.read(from: textView.string)
        doc.save(sender)
    }
    
    override func build() {
        textView.string = doc?.qwerty ?? ""
    }
    
}
