//
//  Word.swift
//  Lexicon
//
//  Created by Neal Watkins on 2020/2/4.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

struct Word {
    
    var title: String
    
    
    func speak() {
        guard let speaker = NSSpeechSynthesizer(voice: Default.voice) else { return }
        speaker.rate = Default.speed
        speaker.startSpeaking(title)
    }
    
    struct Default {
        
        static let defaults = UserDefaults.standard
        
        static var voice: NSSpeechSynthesizer.VoiceName {
            if let voice = defaults.string(forKey: "VoiceID") {
                return NSSpeechSynthesizer.VoiceName(rawValue: voice)
            } else {
                return NSSpeechSynthesizer.defaultVoice
            }
        }
        
        static var speed: Float {
            return defaults.float(forKey: "VoiceRate")
        }
    }
}
